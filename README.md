# Wallpaper manager for multiple desktop

Used on Openbox with feh (or any other command background manager). Change your wallpaper while you change your desktop.

## Require
 - feh (https://feh.finalrewind.org/)
 - xorg (https://www.x.org/wiki/)

## Use
Place in a folder all the background you want to have for each desktop, with the same name/extension, except a number.  
In `wall.sh`, change the var `path` with the path to your folder + name of the file.
In `desWall.sh`, change the path to your `wall.sh` file.

In your `autostart`, or other init/config file, place the path or your file `desWall.sh`.

## Other

You can change the way to name your file by using a condition regarding the number return by xprop (your current desktop).
